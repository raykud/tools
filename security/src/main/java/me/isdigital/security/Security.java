package me.isdigital.security;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.text.TextUtils;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

/**
 * Created by Raykud on 3/2/16.
 */
public abstract class Security {

    // Definición del tipo de algoritmo a utilizar (AES, DES, RSA)
    private final static String ALGORITHM_AES = "AES";
    private final static String ALGORITHM_RSA = "RSA";
    // Definición del modo de cifrado a utilizar
    private final static String CIPHER_INSTANCE = "AES/CBC/PKCS5Padding";
    //Definition of the RSA encryption type name
    private static final String KEY_STORE_TYPE_RSA = "AndroidKeyStore";
    //Definition of the RSA cypher to use
    private final static String RSA_INSTANCE = "RSA/ECB/PKCS1Padding";
    //validity of the RSA key
    private static final int VALIDITY_YEARS = 1;
    //RSA provider
    private static final String RSA_PROVIDER = "AndroidOpenSSL";


    /**
     * This method encrypts based on a predefinded encription key provided by the user appending the base 64 encrypted text
     * with the ivString
     * @param encryptionKey the encryption key that will be used to build a secretkeyscpec
     * @param cleartext the text to be encrypted
     * @return encripted text if its a valid encription key
     * @throws GeneralSecurityException if its an invalid encryption key
     * */
    public static String encryptAES(String cleartext,String encryptionKey) throws GeneralSecurityException{
        if(TextUtils.isEmpty(encryptionKey)){
            throw new GeneralSecurityException("Encryption key cannot be null");
        }
        final byte[] iv = AesCbcWithIntegrity.generateIv();
        final Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
        final SecretKeySpec skeySpec = new SecretKeySpec(encryptionKey.getBytes(), ALGORITHM_AES);
        final IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(cleartext.getBytes());
        final String base64 = Base64.encodeToString(encrypted, Base64.DEFAULT).replaceAll("\\n", "").trim();
        final String ivString = Base64.encodeToString(iv, Base64.DEFAULT).replaceAll("\\n","").trim();
        return ivString+base64;
    }

    /**
     * This method encrypts based on a predefinded encription key provided by the user appending the base 64 encrypted text
     * with the ivString
     * @param encryptionKey the encryption key that will be used to build a secretkeyscpec
     * @param cleartext the text to be encrypted
     * @return encripted text if its a valid encription key
     * @throws GeneralSecurityException if its an invalid encryption key
     * */
    public static String encryptAES(String cleartext,String encryptionKey, String baseIV) throws GeneralSecurityException{
        if(TextUtils.isEmpty(encryptionKey)){
            throw new GeneralSecurityException("Encryption key cannot be null");
        }
        //final byte[] iv = AesCbcWithIntegrity.generateIv();
        final byte[] iv = baseIV.getBytes();
        final Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
        final SecretKeySpec skeySpec = new SecretKeySpec(encryptionKey.getBytes(), ALGORITHM_AES);
        final IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(cleartext.getBytes());
        final String base64 = Base64.encodeToString(encrypted, Base64.DEFAULT).replaceAll("\\n", "").trim();
        final String ivString = Base64.encodeToString(iv, Base64.DEFAULT).replaceAll("\\n","").trim();
        return ivString+base64;
    }

    /**
     * Generates a random IV and encrypts this plain text with the given key. Then attaches
     * a hashed MAC, which is contained in the CipherTextIvMac class.
     *
     * @param plaintext The text that will be encrypted, which
     *                  will be serialized with UTF-8
     * @param secretKeys The AES & HMAC keys with which to encrypt
     * @return a tuple of the IV, ciphertext, mac
     * @throws GeneralSecurityException if AES is not implemented on this system
     * @throws UnsupportedEncodingException if UTF-8 is not supported in this system
     * */
    public static String encryptAES(String plaintext, AesCbcWithIntegrity.SecretKeys secretKeys) throws UnsupportedEncodingException, GeneralSecurityException {
        AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = AesCbcWithIntegrity.encrypt(plaintext, secretKeys);
        //store or send to server
        return cipherTextIvMac.toString();
    }

    /**
     * AES CBC decrypt.
     *
     * @param cipherTextString The cipher text
     * @param keys The AES & HMAC keys
     * @return A string derived from the decrypted bytes, which are interpreted
     *         as a UTF-8 String
     * @throws GeneralSecurityException if AES is not implemented on this system
     * @throws UnsupportedEncodingException if UTF-8 is not supported
     */
    public static String decryptAES(String cipherTextString, AesCbcWithIntegrity.SecretKeys keys) throws UnsupportedEncodingException, GeneralSecurityException {
        //Use the constructor to re-create the CipherTextIvMac class from the string:
        AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = new AesCbcWithIntegrity.CipherTextIvMac(cipherTextString);
        return AesCbcWithIntegrity.decryptString(cipherTextIvMac, keys);
    }


    @SuppressWarnings("deprecation")
    private static Cipher initializeCipherRSA(String alias, boolean encrypt, Context context) throws GeneralSecurityException, IOException, NullPointerException {
        KeyStore keyStore  = KeyStore.getInstance(KEY_STORE_TYPE_RSA);
        keyStore.load(null);
        if (!keyStore.containsAlias(alias)) {
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            end.add(Calendar.YEAR, VALIDITY_YEARS);

            KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                    .setAlias(alias)
                    .setSubject(new X500Principal("CN=Sample Name, O=Android Authority"))
                    .setSerialNumber(BigInteger.ONE)
                    .setStartDate(start.getTime())
                    .setEndDate(end.getTime())
                    .build();
            KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORITHM_RSA, KEY_STORE_TYPE_RSA);
            generator.initialize(spec);
            generator.generateKeyPair();
        }


        final Cipher cipher;
        if(encrypt){
            cipher = Cipher.getInstance(RSA_INSTANCE, RSA_PROVIDER);
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);
            RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        }else{
            cipher = Cipher.getInstance(RSA_INSTANCE);
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);
            PrivateKey privateKey = privateKeyEntry.getPrivateKey();
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
        }
        return cipher;
    }

    public static void deleteRSAKey(String alias) throws GeneralSecurityException, IOException {
        KeyStore keyStore  = KeyStore.getInstance(KEY_STORE_TYPE_RSA);
        keyStore.load(null);
        if (keyStore.containsAlias(alias)) {
            keyStore.deleteEntry(alias);
        }
    }

    public static String encryptRSA(String plainText, String alias, Context context) throws GeneralSecurityException, IOException, NullPointerException {
        if(plainText == null || plainText.isEmpty()) {
            throw  new IOException("text must not be null or empty");
        }

        final Cipher inCipher = initializeCipherRSA(alias,true,context);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inCipher);
        cipherOutputStream.write(plainText.getBytes(StandardCharsets.UTF_8));
        cipherOutputStream.close();

        byte [] vals = outputStream.toByteArray();
        return Base64.encodeToString(vals, Base64.DEFAULT);
    }

    public static String decryptRSA(String encryptedText,String alias, Context context) throws GeneralSecurityException, IOException, NullPointerException {
        final Cipher cipher = initializeCipherRSA(alias,false,context);
        CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(Base64.decode(encryptedText, Base64.DEFAULT)), cipher);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }

        return new String(bytes, 0, bytes.length, StandardCharsets.UTF_8);
    }

    public static String encryptBase64(String plainText) {
        // Sending side
        byte[] data = plainText.getBytes(StandardCharsets.UTF_8);
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decryptBase64(String encryptedBase64) {
        // Receiving side
        byte[] data = Base64.decode(encryptedBase64, Base64.DEFAULT);
        return new String(data, StandardCharsets.UTF_8);
    }

}