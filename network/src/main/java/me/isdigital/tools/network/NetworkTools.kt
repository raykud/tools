package me.isdigital.tools.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import okhttp3.*
import java.io.IOException
import java.text.Normalizer
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Raykud on 4/6/16.
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
object NetworkTools {

    const val REQUEST_STATUS_CODE = "request_status_code"
    const val REQUEST_SERVER_RESPONSE = "server_response"
    const val KEY_ERROR_MESSAGE = "error_message"

    /** Determines if the device is online or not (has an active internet connection)
     * @param mContext the context where it will be looked into.
     * @return a boolean determining the status of the network on the mobile. true if the
     * device has an active internet connection, false otherwise.
     */
    fun isOnline(mContext: Context): Boolean {
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo: NetworkInfo? = cm.activeNetworkInfo
        return netInfo?.isConnected ?: false
    }

    object OperationStatus {
        const val KEY_STATUS = "operation_status"
        const val SUCCESS = 0
        const val SUCCESS_WITH_MESSAGE = 1
        const val FAIL = 2
        const val TOKEN_EXPIRED = 3
        const val SPECIAL_STATUS = 4

        const val NETWORK_ERROR = 5
        const val SERVER_ERROR = 6
        const val APP_ERROR = 7
    }


    /**
     * Created by Raykud on 3/2/16.
     * This class should be extended from your internal NetworkUtils class to be able to use any of the methods.
     * Invocation of the methods are normal inheritance calls.
     * How to use this class: Define a helper class that extends this class and when needing to use the methods
     * just directly use the method with the proper parameters. This class can not be instantiated the reason for this is
     * to force the creation of a helper class that will handle all network operations so when needing to modify a single
     * network operation, there is no need to look for the thread/loader that uses it and instead of modifying per thread
     * will only modify the helper class.
     */


    //operations

    private const val CONNECTION_TIMEOUT = 15 //time in seconds to wait for server to respond;
    private const val SOCKET_TIMEOUT = 15 //time in seconds to wait for data;
    private val JSON = MediaType.parse("application/json; charset=utf-8")

    /**This method builds an internal HTTPClient to perform the requests and is only handled
     * by the internal methods of this class. */
    private//ignore invalid SSL servers
    val okHttpClient: OkHttpClient
        get() {
            val clientBuilder = OkHttpClient.Builder()
            clientBuilder.connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS)
            clientBuilder.readTimeout(SOCKET_TIMEOUT.toLong(), TimeUnit.SECONDS)
            clientBuilder.hostnameVerifier { _, _ -> true }
            return clientBuilder.build()
        }

    /**
     * Creates a GET request with the parameters provided
     * @param endpointInput the endpoint the request will be made to.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataGet(endpointInput: String, headers: HashMap<String, String>? = null): String {
        var endpoint = endpointInput
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
                .url(endpoint)
                .get()
        appendHeaders(headers, requestBuilder)
        val client = okHttpClient
        val request = requestBuilder.build()
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }

    /**
     * Creates a POST request with the parameters provided
     * @param endpoint the endpoint the request will be made to.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataPost(endpoint: String, headers: HashMap<String, String>): String {
        return retrieveDataPost(endpoint, "", headers)
    }

    /**
     * Creates a POST request with the parameters provided
     * @param endpointInput the endpoint the request will be made to.
     * @param jsonInput a properly formatted JSON String that will be sent with the values for the request.
     * This JSON should be built in the helper class.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataPost(endpointInput: String, jsonInput: String? = "", headers: HashMap<String, String>? = null): String {
        var endpoint = endpointInput
        var json = jsonInput
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
                .url(endpoint)
        if (json != null && !json.isEmpty()) {
            json = Normalizer.normalize(json, Normalizer.Form.NFD)
            json = json!!.replace("[^\\p{ASCII}]".toRegex(), "")
            val body = RequestBody.create(JSON, json)
            requestBuilder.post(body)
        } else {
            val body = RequestBody.create(JSON, "{}")
            requestBuilder.post(body)
        }
        appendHeaders(headers, requestBuilder)
        val client = okHttpClient
        val request = requestBuilder.build()
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }


    /**
     * Creates a POST request with the parameters provided, this method is specially created to be used with a MultipartBody.Builder
     * * <pre>`Example:
     * Body.Builder multipartBuilder = new MultipartBody.Builder()
     * .setType(MultipartBody.FORM)
     * .addPart(Headers.of("Content-Disposition", "form-data; name=\"encuesta_id\""), RequestBody.create(null, encuestaId))
     * .addPart(Headers.of("Content-Disposition", "form-data; name=\"encuesta_comentario\""), RequestBody.create(null, comment));
    `</pre> *
     * @param endpointInput the endpoint the request will be made to.
     * @param requestBody a custom RequestBody made for this specific petition. This RequestBody should be built
     * in the helper class. This body is mainly for the multipart elements built in the helper class.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataPost(endpointInput: String, requestBody: RequestBody, headers: HashMap<String, String>? = null): String {
        var endpoint = endpointInput
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
        requestBuilder.url(endpoint)
        requestBuilder.post(requestBody)
        appendHeaders(headers, requestBuilder)
        val request = requestBuilder.build()
        // Open connection
        val client = okHttpClient
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }

    /**
     * Creates a PUT request with the parameters provided
     * @param endpoint the endpoint the request will be made to.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataPut(endpoint: String, headers: HashMap<String, String>): String {
        return retrieveDataPut(endpoint, "", headers)
    }

    /**
     * Creates a PUT request with the parameters provided
     * @param endpointInput the endpoint the request will be made to.
     * @param jsonInput a properly formatted JSON String that will be sent with the values for the request.
     * This JSON should be built in the helper class.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataPut(endpointInput: String, jsonInput: String? = "", headers: HashMap<String, String>? = null): String {
        var endpoint = endpointInput
        var json = jsonInput
        val client = okHttpClient
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
                .url(endpoint)
        if (json != null && !json.isEmpty()) {
            json = Normalizer.normalize(json, Normalizer.Form.NFD)
            json = json!!.replace("[^\\p{ASCII}]".toRegex(), "")
            val body = RequestBody.create(JSON, json)
            requestBuilder.put(body)
        } else {
            val body = RequestBody.create(JSON, "{}")
            requestBuilder.put(body)
        }
        appendHeaders(headers, requestBuilder)
        val request = requestBuilder.build()
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }

    /**
     * Creates a PUT request with the parameters provided, this method is specially created to be used with a MultipartBody.Builder
     * <pre>`Example:
     * Body.Builder multipartBuilder = new MultipartBody.Builder()
     * .setType(MultipartBody.FORM)
     * .addPart(Headers.of("Content-Disposition", "form-data; name=\"encuesta_comentario\""), RequestBody.create(null, comment));
    `</pre> *
     * @param endpointInput the endpoint the request will be made to.
     * @param requestBody a custom RequestBody made for this specific petition. This RequestBody should be built
     * in the helper class. This body is mainly for the multipart elements built in the helper class.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataPut(endpointInput: String, requestBody: RequestBody?, headers: HashMap<String, String>? = null): String? {
        var endpoint = endpointInput
        if (null == requestBody) return null
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
        requestBuilder.url(endpoint)
        requestBuilder.put(requestBody)
        appendHeaders(headers, requestBuilder)
        val request = requestBuilder.build()
        // Open connection
        val client = okHttpClient
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }

    /**
     * Creates a PUT request with the parameters provided
     * @param endpoint the endpoint the request will be made to.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataDelete(endpoint: String, headers: HashMap<String, String>): String {
        return retrieveDataDelete(endpoint, "", headers)
    }

    /**
     * Creates a PUT request with the parameters provided
     * @param endpointInput the endpoint the request will be made to.
     * @param jsonInput a properly formatted JSON String that will be sent with the values for the request.
     * This JSON should be built in the helper class.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataDelete(endpointInput: String, jsonInput: String? = "", headers: HashMap<String, String>? = null): String {
        var endpoint = endpointInput
        var json = jsonInput
        val client = okHttpClient
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
                .url(endpoint)
        if (json != null && !json.isEmpty()) {
            json = Normalizer.normalize(json, Normalizer.Form.NFD)
            json = json!!.replace("[^\\p{ASCII}]".toRegex(), "")
            val body = RequestBody.create(JSON, json)
            requestBuilder.delete(body)
        } else {
            requestBuilder.delete()
        }
        appendHeaders(headers, requestBuilder)
        val request = requestBuilder.build()
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }

    /**
     * Creates a PUT request with the parameters provided, this method is specially created to be used with a MultipartBody.Builder
     * <pre>`Example:
     * Body.Builder multipartBuilder = new MultipartBody.Builder()
     * .setType(MultipartBody.FORM)
     * .addPart(Headers.of("Content-Disposition", "form-data; name=\"encuesta_comentario\""), RequestBody.create(null, comment));
    `</pre> *
     * @param endpointInput the endpoint the request will be made to.
     * @param requestBody a custom RequestBody made for this specific petition. This RequestBody should be built
     * in the helper class. This body is mainly for the multipart elements built in the helper class.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @return a string containing the result from the request that was made to the server
     * @throws IOException if the server is unreachable or if there is an error with the request.
     */
    @Throws(IOException::class)
    fun retrieveDataDelete(endpointInput: String, requestBody: RequestBody?, headers: HashMap<String, String>? = null): String? {
        var endpoint = endpointInput
        if (null == requestBody) return null
        endpoint = endpoint.replace(" ", "%20")
        // Create request for remote resource.
        val requestBuilder = Request.Builder()
        requestBuilder.url(endpoint)
        requestBuilder.delete(requestBody)
        appendHeaders(headers, requestBuilder)
        val request = requestBuilder.build()
        // Open connection
        val client = okHttpClient
        val response = client.newCall(request).execute()
        return buildResponseString(response)
    }

    /**
     * This internal method appends custom headers to the request.
     * @param headers any additional header attached to the request presented in a HashMap,
     * the keys will be the name of the headers and the values will be the value items
     * for the request of such keys.
     * @param requestBuilder the builder that headers will be attached to.
     */
    private fun appendHeaders(headers: HashMap<String, String>?, requestBuilder: Request.Builder) {
        if (headers != null) {
            val headersSize = headers.size
            val keys = ArrayList(headers.keys)
            val values = ArrayList(headers.values)
            for (i in 0 until headersSize) {
                val key = keys[i]
                val value = values[i]
                if (key != null && value != null) {
                    requestBuilder.addHeader(key, value)
                }
            }
        }
    }

    /**
     * this method will build an internal status code JSON with the server response evaluating if
     * the status code is valid or invalid due to a bad request sent from the server */
    @Throws(IOException::class)
    private fun buildResponseString(response: Response): String {
        val code = response.code()
        val statusCode = if (response.isSuccessful) NetworkTools.OperationStatus.SUCCESS else if (code in 400..499) NetworkTools.OperationStatus.FAIL else NetworkTools.OperationStatus.SERVER_ERROR
        val responseBuilder = StringBuilder("{\"")
        responseBuilder.append(NetworkTools.REQUEST_STATUS_CODE)
        responseBuilder.append("\": ")
        responseBuilder.append(statusCode)
        when (statusCode) {
            NetworkTools.OperationStatus.SUCCESS, NetworkTools.OperationStatus.FAIL -> {
                responseBuilder.append(",\"")
                responseBuilder.append(NetworkTools.REQUEST_SERVER_RESPONSE)
                responseBuilder.append("\": ")
                val responseServer = if (response.body() == null) "" else response.body()!!.string()
                if (responseServer.isNotEmpty()) {
                    responseBuilder.append(responseServer)
                } else {
                    responseBuilder.append("\"\"")
                }
                responseBuilder.append("}")
            }
            NetworkTools.OperationStatus.SERVER_ERROR -> throw IOException("Unexpected code $response")
        }
        return responseBuilder.toString()
    }
}
