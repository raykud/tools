package me.isdigital.tools.network

import org.json.JSONObject

fun JSONObject.safeOptString(name :String, fallback: String): String{
    val result = optString(name,fallback)
    return if(result.toLowerCase() == "null") fallback else result
}

fun JSONObject.safeString(name: String): String{
    return safeOptString(name,"")
}