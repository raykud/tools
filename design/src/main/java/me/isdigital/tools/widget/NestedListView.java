/*
 * Copyright 2016 CloudSourceIT. All Rights Reserved.
 * developed by Qiiqe
 */
package me.isdigital.tools.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * An {@link ListView} that can put inside a ScrollView without collapsing.
 */
public class NestedListView extends ListView{

    boolean enabled;

    public NestedListView(Context context){
        this(context, null);
    }

    public NestedListView(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }

    public NestedListView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        // ...because why else would you be using this widget?
        setNestedListViewEnabled(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(isNestedListViewEnabled()){
            // Calculate entire height by providing a very large height hint.
            // View.MEASURED_SIZE_MASK represents the largest height possible.
            int newHeight = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, newHeight);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
        }else{
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void setNestedListViewEnabled(boolean enabled){
        this.enabled = enabled;
    }

    public boolean isNestedListViewEnabled(){
        return enabled;
    }

}
