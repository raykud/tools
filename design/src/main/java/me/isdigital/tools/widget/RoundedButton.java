package me.isdigital.tools.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import me.isdigital.tools.R;

/***
 * A custom Button widget that will have its corners rounded.
 */
public class RoundedButton extends AppCompatButton {

    RectF rect = new RectF(0, 0, 0, 0);
    Path clipPath = new Path();
    private float mRadius = 0f;

    public RoundedButton(Context context) {
        super(context);
    }

    public RoundedButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        // retrieve styles attributes
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RoundedButton);
        mRadius = a.getDimension(R.styleable.RoundedButton_corner_radius, 0f);
        a.recycle();
    }

    public void setRadius(float mRadius){
        if(mRadius>1){
            this.mRadius = mRadius;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // only do this if we actually have a radius
        if (mRadius > 0) {
            rect.set(0, 0, getWidth(), getHeight());
            clipPath.addRoundRect(rect, mRadius, mRadius, Path.Direction.CW);
            canvas.clipPath(clipPath);
        }
        super.onDraw(canvas);
    }
}
