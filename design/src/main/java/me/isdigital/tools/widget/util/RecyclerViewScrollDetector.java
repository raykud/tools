/*
 * Copyright 2016 CloudSourceIT. All Rights Reserved.
 * developed by Qiiqe
 */
package me.isdigital.tools.widget.util;

import android.support.v7.widget.RecyclerView;

/**
 * see {@link me.isdigital.tools.widget.ObservableFloatingActionButton}
 */
public abstract class RecyclerViewScrollDetector extends RecyclerView.OnScrollListener {
    private int mScrollThreshold;

    public abstract void onScrollUp();

    public abstract void onScrollDown();

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        boolean isSignificantDelta = Math.abs(dy) > mScrollThreshold;
        if (isSignificantDelta) {
            if (dy > 0) {
                onScrollUp();
            } else {
                onScrollDown();
            }
        }
    }

    public void setScrollThreshold(int scrollThreshold) {
        mScrollThreshold = scrollThreshold;
    }
}
