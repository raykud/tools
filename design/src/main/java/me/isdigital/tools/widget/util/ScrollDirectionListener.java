/*
 * Copyright 2016 CloudSourceIT. All Rights Reserved.
 * developed by Qiiqe
 */
package me.isdigital.tools.widget.util;

/**
 * see {@link me.isdigital.tools.widget.ObservableFloatingActionButton}
 */
public interface ScrollDirectionListener {
    void onScrollDown();

    void onScrollUp();
}
