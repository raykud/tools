/*
 * Copyright 2016 CloudSourceIT. All Rights Reserved.
 * developed by Qiiqe
 */
package me.isdigital.tools.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import me.isdigital.tools.R;
import me.isdigital.tools.widget.util.RecyclerViewScrollDetector;
import me.isdigital.tools.widget.util.ScrollDirectionListener;


public class ObservableFloatingActionButton extends FloatingActionButton{

    private static final int TRANSLATE_DURATION_MILLIS = 200;

    private boolean mVisible = true;
    private final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();
    private int mScrollThreshold;

    public ObservableFloatingActionButton(Context context){
        this(context, null);
    }

    public ObservableFloatingActionButton(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }

    public ObservableFloatingActionButton(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        mScrollThreshold = getResources().getDimensionPixelOffset(R.dimen.fab_scroll_threshold);
    }

    public boolean isVisible(){
        return mVisible;
    }

    public void show(){
        show(true);
    }

    public void hide(){
        hide(true);
    }

    public void show(boolean animate){
        toggle(true, animate, false);
    }

    public void hide(boolean animate) {
        toggle(false, animate, false);
    }

    private void toggle(final boolean visible, final boolean animate, boolean force) {
        if (mVisible != visible || force) {
            mVisible = visible;
            int height = getHeight();
            if (height == 0 && !force) {
                ViewTreeObserver vto = getViewTreeObserver();
                if (vto.isAlive()) {
                    vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ViewTreeObserver currentVto = getViewTreeObserver();
                            if (currentVto.isAlive()) {
                                currentVto.removeOnPreDrawListener(this);
                            }
                            toggle(visible, animate, true);
                            return true;
                        }
                    });
                    return;
                }
            }
            int translationY = visible ? 0 : height + getMarginBottom();
            if (animate) {
                ViewCompat.animate(this).setInterpolator(mInterpolator)
                        .setDuration(TRANSLATE_DURATION_MILLIS)
                        .translationY(translationY);

            } else {
                setTranslationY(translationY);
            }
        }
    }

    /**
     * @param recyclerView a RecyclerView that can accept a scroll listener.
     * if scroll up {@link #hide()}.
     * if scroll down {@link #show()}.
     **/
    public void attachToRecyclerView(@NonNull RecyclerView recyclerView){
        attachToRecyclerView(recyclerView, null, null);
    }

    public void attachToRecyclerView(@NonNull RecyclerView recyclerView,
                                     ScrollDirectionListener scrollDirectionlistener,
                                     RecyclerView.OnScrollListener onScrollListener) {

        RecyclerViewScrollDetectorImpl scrollDetector = new RecyclerViewScrollDetectorImpl();
        scrollDetector.setScrollDirectionListener(scrollDirectionlistener);
        scrollDetector.setOnScrollListener(onScrollListener);
        scrollDetector.setScrollThreshold(mScrollThreshold);
        recyclerView.addOnScrollListener(scrollDetector);
    }

    private class RecyclerViewScrollDetectorImpl extends RecyclerViewScrollDetector {
        private ScrollDirectionListener mScrollDirectionListener;
        private RecyclerView.OnScrollListener mOnScrollListener;

        private void setScrollDirectionListener(ScrollDirectionListener scrollDirectionListener) {
            mScrollDirectionListener = scrollDirectionListener;
        }

        @SuppressWarnings("WeakerAccess")
        public void setOnScrollListener(RecyclerView.OnScrollListener onScrollListener) {
            mOnScrollListener = onScrollListener;
        }

        @Override
        public void onScrollDown() {
            show();
            if (mScrollDirectionListener != null) {
                mScrollDirectionListener.onScrollDown();
            }
        }

        @Override
        public void onScrollUp() {
            hide();
            if (mScrollDirectionListener != null) {
                mScrollDirectionListener.onScrollUp();
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(mOnScrollListener != null){
                mOnScrollListener.onScrolled(recyclerView, dx, dy);
            }
            super.onScrolled(recyclerView, dx, dy);
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (mOnScrollListener != null) {
                mOnScrollListener.onScrollStateChanged(recyclerView, newState);
            }
            super.onScrollStateChanged(recyclerView, newState);
        }
    }

    private int getMarginBottom() {
        int marginBottom = 0;
        final ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            marginBottom = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }
        return marginBottom;
    }
}
