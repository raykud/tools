package me.isdigital.tools.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;

/**
 * This is a custom Button Widget to be used to align the drawable with the text so its not close to the bounds but rather
 * to the bounds of the text.
 * **/
public class CenteredIconButton extends AppCompatButton {
    private static final int LEFT = 0, TOP = 1, RIGHT = 2, BOTTOM = 3;

    // Pre-allocate objects for layout measuring
    private Rect textBounds = new Rect();
    private Rect drawableBounds = new Rect();

    public CenteredIconButton(Context context) {
        this(context, null);
    }

    public CenteredIconButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.buttonStyle);
    }

    public CenteredIconButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (!changed) return;

        final CharSequence text = getText();
        if (!TextUtils.isEmpty(text)) {
            TextPaint textPaint = getPaint();
            textPaint.getTextBounds(text.toString(), 0, text.length(), textBounds);
        } else {
            textBounds.setEmpty();
        }

        final int width = getWidth() - (getPaddingLeft() + getPaddingRight());
        final int height = getHeight() - (getPaddingTop() + getPaddingBottom());

        final Drawable[] drawables = getCompoundDrawables();

        if (drawables[LEFT] != null) {
            drawables[LEFT].copyBounds(drawableBounds);
            int leftOffset = (width - (textBounds.width() + drawableBounds.width()) + getRightPaddingOffset()) / 2 - getCompoundDrawablePadding();
            drawableBounds.offset(leftOffset, 0);
            drawables[LEFT].setBounds(drawableBounds);
        }

        if (drawables[RIGHT] != null) {
            drawables[RIGHT].copyBounds(drawableBounds);
            int rightOffset = ((textBounds.width() + drawableBounds.width()) - width + getLeftPaddingOffset()) / 2 + getCompoundDrawablePadding();
            drawableBounds.offset(rightOffset, 0);
            drawables[RIGHT].setBounds(drawableBounds);
        }
        
        if (drawables[TOP] != null) {
            drawables[TOP].copyBounds(drawableBounds);
            int topOffset = (height -(textBounds.height() + drawableBounds.height()) + getTopPaddingOffset()) / 2 - getCompoundDrawablePadding();
            drawableBounds.offset(0, topOffset);
            drawables[TOP].setBounds(drawableBounds);
        }
        
        if (drawables[BOTTOM] != null) {
            drawables[BOTTOM].copyBounds(drawableBounds);
            int bottomOffset = ((textBounds.height() + drawableBounds.height()) - height + getBottomPaddingOffset()) / 2 + getCompoundDrawablePadding();
            drawableBounds.offset(0, bottomOffset);
            drawables[BOTTOM].setBounds(drawableBounds);
        }
    }
}