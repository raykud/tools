package me.isdigital.tools.bitmaptransform;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;

public class Blur {


	/** Call this method when wanting to apply a blur to a Bitmap
	 * @param context the context
	 * @param sentBitmap the bitmap that will recive the blur effect
     * @param radius the radius of bluring
     * @return the bitmap with the effect applied.*/
	public static Bitmap fastblur(Context context, Bitmap sentBitmap, int radius) {

		Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

		final RenderScript rs = RenderScript.create(context);
		final Allocation input = Allocation.createFromBitmap(rs, sentBitmap, Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
		final Allocation output = Allocation.createTyped(rs, input.getType());
		final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
		script.setRadius(radius /* e.g. 3.f */);
		script.setInput(input);
		script.forEach(output);
		output.copyTo(bitmap);
		return bitmap;
	}

}
